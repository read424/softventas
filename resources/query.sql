--PRODUCTO CON MAS STOCK
SELECT prod.* 
FROM producto AS prod 
ORDER BY prod.stock DESC 
LIMIT 0, 1;

--PRODUCTO MAS VENDIDO
SELECT SUM(dv.cantidad) AS cnt_vendida, dv.id_producto, p.codigo , p.nombre 
FROM detalle_ventas dv
LEFT OUTER JOIN producto p ON p.id=dv.id_producto 
GROUP BY dv.id_producto, p.id 
ORDER BY 1 DESC 
LIMIT 0, 1;