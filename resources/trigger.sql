CREATE DEFINER=`root`@`%` TRIGGER `decrement_stock` 
BEFORE INSERT ON `detalle_ventas` FOR EACH ROW BEGIN
    DECLARE lc_stock INT;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    SELECT pro.stock INTO lc_stock FROM producto AS pro WHERE pro.id = NEW.id_producto;
    IF NEW.cantidad>lc_stock THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "La cantidad a registrar es superior a disponible en stock.";
    ELSE
        UPDATE producto SET stock=stock-NEW.cantidad WHERE id = NEW.id_producto;
    END IF;
END