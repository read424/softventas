CREATE DATABASE `db_softventas` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

CREATE TABLE `categoria` (
  `id` int NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `desc_categoria_UN` (`descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

-- db_softventas.producto definition

CREATE TABLE `producto` (
  `id` int NOT NULL AUTO_INCREMENT,
  `codigo` varchar(10) COLLATE utf8mb4_general_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `precio` int NOT NULL DEFAULT '0',
  `peso` int NOT NULL DEFAULT '0',
  `id_categoria` int NOT NULL,
  `stock` int NOT NULL DEFAULT '0',
  `fec_registro` date NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

-- db_softventas.ventas definition

CREATE TABLE `ventas` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

-- db_softventas.detalle_ventas definition

CREATE TABLE `detalle_ventas` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_venta` int NOT NULL,
  `id_producto` int NOT NULL DEFAULT '1',
  `precio` int NOT NULL DEFAULT '0',
  `cantidad` int NOT NULL,
  `total` double NOT NULL DEFAULT '0',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE DEFINER=`root`@`%` TRIGGER `decrement_stock` BEFORE INSERT ON `detalle_ventas` FOR EACH ROW BEGIN
    DECLARE lc_stock INT;
    SELECT pro.stock INTO lc_stock FROM producto AS pro WHERE pro.id = NEW.id_producto;
    IF NEW.cantidad>lc_stock THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "La cantidad a registrar es superior a disponible en stock.";
    ELSE
        UPDATE producto SET stock=stock-NEW.cantidad WHERE id = NEW.id_producto;
    END IF;
END;
