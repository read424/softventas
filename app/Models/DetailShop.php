<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class DetailShop extends Model{
    const CREATED_AT = 'create_at';
    const UPDATED_AT = 'update_at';

    protected $table = 'detalle_ventas';
    protected $primaryKey = 'id';
    protected $fillable=['id_venta', 'id_producto', 'precio', 'cantidad', 'total'];
    public $incrementing = true;
    protected $hidden = ['create_at', 'update_at'];

    public function venta()
    {
        return $this->belongsTo(Shop::class, 'id_venta');
    }

    public function producto()
    {
        return $this->belongsTo(Product::class, 'id_producto');
    }
}