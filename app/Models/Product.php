<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Product extends Model{
    const CREATED_AT = 'create_at';
    const UPDATED_AT = 'update_at';

    protected $table = 'producto';
    protected $primaryKey = 'id';
    protected $fillable=['codigo', 'nombre', 'precio', 'peso', 'id_categoria', 'stock', 'fec_registro'];
    public $incrementing = true;
    protected $hidden = ['create_at', 'update_at'];

    public function categoria()
    {
        return $this->belongsTo(Categoria::class, 'id_categoria', 'id');
    }    
}