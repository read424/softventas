<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Shop extends Model{
    const CREATED_AT = 'create_at';
    const UPDATED_AT = 'update_at';

    protected $table = 'ventas';
    protected $primaryKey = 'id';
    protected $fillable=[];
    public $incrementing = true;
    protected $hidden = ['create_at', 'update_at'];

    public function categoria()
    {
        return $this->belongsTo(Categoria::class, 'id_categoria', 'id');
    }    
}