<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Categoria extends Model{
    const CREATED_AT = 'create_at';
    const UPDATED_AT = 'update_at';

    protected $table = 'categoria';
    protected $primaryKey = 'id';
    protected $fillable=['descripcion'];
    protected $hidden = ['create_at', 'update_at'];
    public $incrementing = true;
    
    /**
     * The product that belong to the categoria.
     */
    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class);
    }    

}