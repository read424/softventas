<?php
namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class ProductController extends Controller {
    
    public function index(){
        $products = Product::with('categoria')->get();
        return response()->json($products);
    }

    public function show($id){
        $product = Product::with('categoria')->find($id);
        return response()->json($product);
    }

    public function create(Request $request){
        try{
            $validator= Validator::make($request->all(),[
                'descripcion' => 'required|string|max:100|unique:producto,nombre',
                'referencia' => 'required|string|max:10|unique:producto,codigo',
                'categoria.value' => 'required|integer',
                'precio' => 'required|integer',
                'peso' => 'required|integer',
                'cnt_stock' => 'required|integer',
                'fec_ingreso' => 'required|date'
            ]);
            if ($validator->fails())
                return response()->json(['error'=>$validator->errors()->first()],400);
            $product = new Product();
            $product->codigo=$request->input('referencia');
            $product->nombre=$request->input('descripcion');
            $product->precio=$request->input('precio');
            $product->peso=$request->input('peso');
            $product->id_categoria=$request->input('categoria.value');
            $product->stock=$request->input('cnt_stock');
            $fecha_ingreso=\DateTime::createFromFormat('Y-m-d\TH:i:s.uP', $request->input('fec_ingreso'));
            $product->fec_registro=$fecha_ingreso->format('Y-m-d');
            $product->save();
            return response()->json($product);
        }catch(\Throwable $e){
            return response()->json([
                'error'=>$e->getMessage()
            ], 500);
        }
    }

    public function update(Request $request, $id){
        try{
            $product = Product::with('categoria')->find($id);
            $validator= Validator::make($request->all(), [
                'id'=>'required|integer',
                'descripcion' => 'required|string|max:100|unique:producto,nombre,'.$id,
                'referencia' => 'required|string|max:10|unique:producto,codigo,'.$id,
                'categoria.value' => 'required|integer',
                'precio' => 'required|integer',
                'peso' => 'required|integer',
                'fec_ingreso' => 'required|date',
            ], ['required'=>'El dato :attribute es obligatorio', 'integer'=>'El :attribute debe ser un valor entero', 'unique'=>'El valor para :attribute ya existe para otro registro']);
            if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()->first()],400);
            }
            $product->codigo=$request->input('referencia');
            $product->nombre=$request->input('descripcion');
            $product->id_categoria=$request->input('categoria.value');
            $product->precio=$request->input('precio');
            $product->peso=$request->input('peso');
            $product->save();
            return response()->json($product);
        }catch(\Throwable $e){

        }
    }

    public function delete($id){
        $product = Product::find($id);
    }

    public function validar(Request $request, $id=0){
        try{
            $this->validate($request,[
                'value' => 'required|string',
                'type' => 'required|string|in:descripcion,referencia',
            ]);
            $descripcion = $request->input('value');
            $name_column = ($request->input('type')=='referencia')?'codigo':'nombre';
            $id = (int) $id;
            $product = Product::where([
                [$name_column, '=', $request->input('value')],
                ['id', '!=', $id]
            ])->get()->first();
            if(!isset($product->id))
                return response()->json(['message'=>'Descripción disponible']);
            else
                return response()->json(['message'=>'Dato invalido para registrar'], 400);
        }catch(\Throwable $e){
            return response()->json([
                'error'=>$e->getMessage()
            ], 500);
        }
    }

}