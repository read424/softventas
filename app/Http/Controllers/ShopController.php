<?php
namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Models\Shop;
use App\Models\DetailShop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Validator;

class ShopController extends Controller {
    
    public function create(Request $request){
        try{
            DB::beginTransaction();
            $shop = new Shop();
            $shop->save();
            $validator= Validator::make($request->all(), [
                'data'=>'required|array',
                'data.*.id'=>'required|integer',
                'data.*.cantidad'=>'required|integer',
                'data.*.precio'=>'required|integer',
                'data.*.total'=>'required|integer'
            ]);
            if ($validator->fails())
                return response()->json(['error'=>'Error de validación', 'messasge'=>$validator->errors()],400);
            $details=$request->input('data');
            foreach($details as $row_producto){
                $detalle = new DetailShop();
                $detalle->id_venta=$shop->id;
                $detalle->id_producto=$row_producto['id'];
                $detalle->precio=$row_producto['precio'];
                $detalle->cantidad=$row_producto['cantidad'];
                $detalle->total=$row_producto['total'];
                $detalle->save();
            }
            DB::commit();
            return response()->json(['message'=>'La venta fue guardada con éxito']);
        }catch(QueryException $e){
            DB::rollBack();
            $errorCode = $e->getCode(); // Código de error de la excepción
            $errorMessage = $e->getMessage(); // Mensaje de error
            if($errorCode==45000)
                $errorMessage="Uno de los articulos se quedo sin stock";
            return response()->json([
                'error'=>$errorMessage
            ], 400);
        }catch(\Throwable $e){
            DB::rollBack();
            return response()->json([
                'error'=>$e->getMessage()
            ], 500);
        }
    }
}