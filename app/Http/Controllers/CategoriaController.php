<?php
namespace App\Http\Controllers;

use App\Models\Categoria;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class CategoriaController extends Controller {
    
    public function index(){
        try{
            $categorias = Categoria::all();
            return response()->json($categorias);    
        }catch(\Throwable $e){
            return response()->json([
                'error'=>[
                    'descripcion'=>$e->getMessage()
                ]
            ], 500);
        }
    }

    public function show($id){
        try{
            $categoria = Categoria::find($id);
            if(empty($categoria))
                return response()->json(['message'=>'No existe el registro a consultar'], 400);
            else
                return response()->json($categoria);
        }catch(\Throwable $e){
            return response()->json([
                'error'=>$e->getMessage(), 'id'=>$id
                ], 500);
        }
    }

    public function create(Request $request){
        try{
            $this->validate($request,[
                'descripcion' => 'required|string|max:100|unique:categoria',
            ]);
            $categoria = new Categoria();
            $categoria->descripcion = $request->input('descripcion');
            $categoria->save();
            return response()->json(['message'=>'Registro creado con éxito']);
        }catch(\Throwable $e){
            return response()->json([
                'error'=>$e->getMessage()
            ], 500);
        }
    }

    public function update(Request $request, $id){
        try{
            $categoria = Categoria::find($id);

            $validator= Validator::make($request->all(), [
                'id'=>'required|integer',
                'descripcion' => 'required|string|max:100|unique:categoria,descripcion,'.$id,
            ], ['required'=>'El dato :attribute es obligatorio', 'integer'=>'El :attribute debe ser un valor entero', 'unique'=>'El valor para :attribute ya existe para otro registro']);

            if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()->first()],400);
            }
            $categoria->descripcion=$request->input('descripcion');
            $categoria->save();
            return response()->json(['data'=>$categoria]);
        }catch(\Throwable $e){
            return response()->json([
                'error'=>$e->getMessage()
            ], 500);
        }
    }

    public function delete($id){
        $categoria = Categoria::find($id);
    }

    public function validar(Request $request, $id=0){
        try{
            $this->validate($request,[
                'descripcion' => 'required|string|max:100',
            ]);
            $descripcion = $request->input('descripcion');
            $id = (int) $id;
            $categoria = DB::select('select * from categoria where descripcion=? AND id!=?', [$descripcion, $id]);
            if(count($categoria)==0)
                return response()->json(['message'=>'Descripción disponible']);
            else
                return response()->json(['message'=>'Ya existe una categoria con esta descripcion'], 400);
        }catch(\Throwable $e){
            return response()->json([
                'error'=>[
                    'descripcion'=>$e->getMessage(),
                    'id'=>$id
                ]
            ], 500);
        }
    }
}