<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// $router->get('/', function () use ($router) {
//     return $router->app->version();
// });
// $router->get('/key', function() {
//     return \Illuminate\Support\Str::random(32);
// });
$router->get('/categoria', 'CategoriaController@index');
$router->post('/categoria', 'CategoriaController@create');
$router->get('/categoria/{id}', 'CategoriaController@show');
$router->put('/categoria/{id}', 'CategoriaController@update');
$router->delete('/categoria/{id}', 'CategoriaController@delete');

$router->post('/validar/categoria[/{id}]', 'CategoriaController@validar');

$router->get('/producto', 'ProductController@index');
$router->post('/producto', 'ProductController@create');
$router->get('/producto/{id}', 'ProductController@show');
$router->put('/producto/{id}', 'ProductController@update');
$router->delete('/producto/{id}', 'ProductController@delete');

$router->post('/validar/producto[/{id}]', 'ProductController@validar');

$router->post('/eshop', 'ShopController@create');
